'use strict';

const path = require('path');
const webpack = require('webpack');

module.exports = environment => {
  const ifProd = plugin => environment.production ? plugin : null;
  const removeEmpty = array => array.filter(entry => !!entry);

  return {
    bail: !!environment.production,
    context: path.resolve(__dirname, 'src'),
    devtool: environment.production ? 'cheap-module-source-map' : 'eval-source-map',
    entry: {
      index: path.resolve(__dirname, 'src', 'index.ts')
    },
    module: {
      rules: [
        {
          test: /\.ts$/,
          use: [ 'babel-loader', 'ts-loader' ]
        }
      ]
    },
    output: {
      chunkFilename: environment.cache ? '[name].[chunkhash].js' : '[name].js',
      filename: environment.cache ? '[name].[chunkhash].js' : '[name].js',
      library: 'OutlaymanApi',
      libraryTarget: 'umd',
      path: path.resolve(__dirname, 'dist')
    },
    plugins: removeEmpty([
      ifProd(new webpack.DefinePlugin({
        'process.env': { 'NODE_ENV': 'production '}
      })),
      ifProd(new webpack.optimize.UglifyJsPlugin({
        compress: {
          screw_ie8: true,
          warnings: false
        }
      }))
    ]),
    resolve: {
      extensions: [ '.js', '.ts' ]
    },
    target: 'web',
    watch: !!environment.development
  }
};
