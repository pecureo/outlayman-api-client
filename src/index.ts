import { AES } from 'crypto-js';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { stringify as queryStringify } from 'qs';
import 'fetch-polyfill';

import 'rxjs/add/observable/empty';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/mergeMap';

interface LinkCodeAuthorizationData {
  expires_at: number;
  token: string;
};

interface WalletData {
  id: number,
  description: string
}

export class OutlaymanApi {
  public error$: Subject<Error> = new Subject();

  private _request: (
    endpoint: string, method: string, params: object, body: object
  ) => Promise<string>;

  constructor(private host: string, authorization_token: string = '') {
    this.setAuthorizationToken(authorization_token);
  }

  announcement$() {
    return this.request('/announcement')
      .map((response: AnnouncementData[]) => response.map(announcement_data =>
        new Announcement(this, announcement_data)
      ));
  }

  createIdentifierLinkCode(
    authorization_data: LinkCodeAuthorizationData,
    wallet: WalletData,
    identifier_description: string,
    password: string = ''
  ) {
    const link_code_data = {
      expires_at: authorization_data.expires_at,
      token: `Bearer ${authorization_data.token}`,
      uri: `${this.host}/wallet/${wallet.id}/link`,
      description: identifier_description,
      wallet_description: wallet.description
    };

    const identifier_link_code = AES.encrypt(
      JSON.stringify(link_code_data), password
    ) .toString();
    const link_code_id = identifier_link_code.replace(/[^a-z0-9]+/gi, '').substr(0, 25);

    return this.request('/identifier_link_code', 'post', {}, {
      expires_in: Math.floor(authorization_data.expires_at - (Date.now() / 1000)),
      id: link_code_id,
      identifier_link_code
    })
    .mergeMap((response: ApiResponse) => {
      if(response.error) {
        this.error$.next(new Error(response.error_description));

        return Observable.empty();
      } else {
        return Observable.of(link_code_id);
      }
    });
  }

  createTransaction(
    amount: number,
    benefactor: number,
    beneficiary: number,
    description: object = null
  ) {
    return this.request('/transaction', 'post', {}, {
      amount, benefactor, beneficiary, description
    })
    .mergeMap((response: TransactionData) => {
      if(response.error) {
        this.error$.next(new TransactionError(
          amount, benefactor, beneficiary, description, response.error
        ));

        return Observable.empty();
      } else {
        return Observable.of(new Transaction(this, response));
      }
    });
  }

  register(
    description: string
  ) {
    return this.request('/credential/register', 'post', {}, {
      description
    })
    .mergeMap((response: ApiResponse) => {
      if(response.error) {
        this.error$.next(new Error(response.error));

        return Observable.empty();
      } else {
        return Observable.of(response.success);
      }
    });
  }

  request(
    endpoint: string = '',
    method: string = 'get',
    params: object = {},
    body: object = {}
  ) {
    let response = this._request(endpoint, method, params, body);

    return Observable.empty()
      .merge(Observable.fromPromise(response).catch(error => {
        this.error$.next(error);

        return Observable.empty();
      }));
  }

  setAuthorizationToken(authorization_token: string) {
    this._request = async (
      endpoint: string = '',
      method: string = 'get',
      params: object = {},
      body: object = {}
    ) => {
      let uri = `${this.host}${endpoint}`;

      let payload = {
        headers: { Authorization: `Bearer ${authorization_token}` },
        method
      };

      if (Object.keys(params).length > 0) {
        if (endpoint.indexOf('?') < 0) {
          uri += `?${queryStringify(params, { arrayFormat: 'brackets' })}`;
        } else {
          uri += `&${queryStringify(params, { arrayFormat: 'brackets' })}`;
        }
      }

      if (Object.keys(body).length > 0) {
        payload['body'] = JSON.stringify(body);
        payload.headers['Content-Type'] = 'application/json; charset=utf-8';
      }

      let response = await fetch(uri, payload);

      return response.json();
    };
  }

  credential$(id: string) {
    return this.request(`/credential/${id}`)
      .map((credential_data: CredentialData) =>
        new Credential(this, credential_data)
      );
  }
}

export class Announcement {
  public createdAt: Date;
  public description: string;
  public expiresAt: Date;
  public id: number;
  public title: string;
  public updatedAt: Date;
  public url: string;

  constructor(private _api: OutlaymanApi, private _data: AnnouncementData) {
    this.createdAt = new Date(this._data.createdAt);
    this.description = this._data.description;
    this.expiresAt = new Date(this._data.expires);
    this.id = this._data.id;
    this.title = this._data.title;
    this.updatedAt = new Date(this._data.updatedAt);
    this.url = this._data.url;
  }
}

interface AnnouncementData {
  createdAt: string;
  description: string;
  expires: string;
  id: number;
  title: string;
  updatedAt: string;
  url: string;
}

export class Credential {
  public id: string;
  public description: string;
  public wallets: Wallet[];

  constructor(private _api: OutlaymanApi, private _data: CredentialData) {
    this.id = this._data.uuid;
    this.description = this._data.description;

    this.wallets = this._data.wallets.map(
      (wallet_data: WalletData) => new Wallet(this._api, wallet_data)
    );
  }
}

interface ApiResponse {
  error?: string;
  error_description?: string;
  success?: string;
}

interface CredentialData {
  description: string
  uuid: string;
  wallets: WalletData[];
}

export class Transaction {
  amount: number;
  id: number;
  benefactor: number;
  beneficiary: number;
  createdAt: Date;
  description: object;

  constructor(private _api: OutlaymanApi, private _data: TransactionData) {
    this.id = this._data.id;
    this.amount = this._data.amount;
    this.benefactor = this._data.benefactor;
    this.beneficiary = this._data.beneficiary;
    this.createdAt = new Date(this._data.createdAt);
    this.description = this._data.description;
  }
}

export class TransactionError {
  public name: string = 'Transaction Error';

  constructor(
    public amount: number,
    public benefactor: number,
    public beneficiary: number,
    public description: object,
    public message: string
  ) {}
}

interface TransactionData {
  amount: number;
  id: number;
  benefactor: number;
  beneficiary: number;
  createdAt: string;
  description: object;

  error?: string;
}

export class Wallet {
  public description: string;
  public id: number;

  constructor(private _api: OutlaymanApi, private _data: WalletData) {
    this.description = this._data.description;
    this.id = this._data.id;
  }
}

interface WalletData {
  description: string;
  id: number;
}
